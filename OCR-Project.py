import logging
import os
import pytesseract
from PIL import Image

logging.basicConfig(level=logging.ERROR)

def extract_text_from_image(image_path):
    """
    Extracts text from an image using pytesseract.
    """
    try:
        # Open the image file
        image = Image.open(image_path)
    except IOError:
        logging.error(f"Error opening or loading image file {image_path}")
        return ""

    try:
        # Extract text from image
        text = pytesseract.image_to_string(image)
        return text
    except pytesseract.pytesseract.TesseractError as e:
        logging.error(f"Error extracting text from image file {image_path}: {e}")
        return ""

if __name__ == '__main__':
    # Prompt the user to enter the path to the image file
    image_path = input('Enter the path to the image file: ')

    # Validate the image file path
    if not os.path.exists(image_path):
        print("Error: Image file does not exist")
        sys.exit()
    elif not image_path.endswith('.jpg') and not image_path.endswith('.png'):
        print("Error: Image file format not supported")
        sys.exit()

    # Extract text from image
    text = extract_text_from_image(image_path)

    # Print the extracted text
    print(text)
